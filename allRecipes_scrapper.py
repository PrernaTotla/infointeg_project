from bs4 import BeautifulSoup
from urllib2 import urlopen
import re
import json
import sys

url_prefix = "http://allrecipes.com/recipes/main.aspx?Page="
url_suffix = "#recipes"

def getRecipeURLs(soup):
	recipeURLS = []
	for a_tag in soup.find_all('a'):
		if a_tag.get('href') != None and a_tag.get('class') != None and 'img-link' in a_tag.get('class'):
			recipeURL = "http://allrecipes.com" + a_tag.get('href')
			#print recipeURL
			recipeURLS.append(recipeURL)
	return recipeURLS

def getRecipeDetails(recipeURL):
	#print recipeURL
	recipeDetails = {}
	html_doc = urlopen(recipeURL)
	if html_doc != None:
		soup = BeautifulSoup(html_doc)
		#Title
		for h1 in soup.find_all('h1'):
			if h1.get('id') != None and h1.get('id') == 'itemTitle':
				#print "Recipe Name: " + h1.contents[0]
				recipeDetails['recipe_name'] = h1.contents[0]
				break
		#Prep time
		for span in soup.find_all('span'):
			if span.get('class') != None and 'time' in span.get('class'):
				#print "Time to cook: " + span.em.contents[0] + " " + span.contents[len(span.contents)-1].strip()
				recipeDetails['prep_time'] = span.em.contents[0] + " " + span.contents[len(span.contents)-1].strip()
				break
		#Ingredients
		recipeDetails['ingredients'] = []
		for p in soup.find_all('p'):
			if p.get('class') != None and 'fl-ing' in p.get('class') and len(p.find_all('span')) > 1:
				#ingred = ""
				ingred = {}
				for span in p.find_all('span'):
					if span.get('id') != None and span.get('id') == 'lblIngAmount':
						#ingred += ", Amount: " + span.contents[0]
						ingred['amount'] = span.contents[0]
					if span.get('id') != None and span.get('id') == 'lblIngName':
						#ingred = "Ingredient: " + span.contents[0] + ingred
						ingred['ingredient'] = span.contents[0]
				if len(ingred) > 1:
					recipeDetails['ingredients'].append(ingred)
				#print ingred
		#Steps
		recipeDetails['prep_steps'] = []
		for div in soup.find_all('div'):
			#steps = []
			if div.get('itemprop') != None and div.get('itemprop') == 'recipeInstructions' and div.ol != None:
				for li in div.ol.find_all('li'):
					#steps.append(str(li.span.contents[0]))
					recipeDetails['prep_steps'].append(li.span.contents[0])
			#if len(steps) > 0:
			#	print steps
	return recipeDetails

global_count = 20329
output_prefix = "../allRecipesData/"
i = 1017
while i <= 2700:
	url = url_prefix + str(i) + url_suffix
	#print url
	print i
	html_doc = urlopen(url)
	if html_doc != None:
		soup = BeautifulSoup(html_doc)
		recipeURLS = getRecipeURLs(soup)
		for recipe in recipeURLS:
			details = getRecipeDetails(recipe)
			if len(details) > 0:
				with open(output_prefix+str(global_count)+".json",'w') as outf:
					json.dump(details,outf,indent=4,sort_keys=True)
				global_count += 1
	i += 1

