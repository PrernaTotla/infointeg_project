import sys
import json
import re

inputfile = sys.argv[1]
outputfile = sys.argv[2]
reference = sys.argv[3]


reference_tokens = []
ref_file = open(reference,'r')
line = ref_file.readline()
while line != None and line != '':
	tokens = line.strip().split()
	for tok in tokens:
		if tok not in reference_tokens:
			reference_tokens.append(tok)
	line = ref_file.readline()
ref_file.close()

data = []
with open(inputfile,'r') as f:
	data = json.load(f)

def getListOfSubstitutes(text):
	sublist = []
	if ' plus ' in text:
		sublist = text.split(' plus ')
	elif ' and ' in text:
		sublist = text.split(' and ')
	else:
		sublist.append(text)
	return sublist

def separateProperties(text):
	text = text.lower()
	text = text.replace(","," , ")
	name = ""
	prop = ""
	tokens = text.split()
	for tok in tokens:
		found = False
		for ref in reference_tokens:
			if tok == ref or tok == ref+"s" or tok == ref+"es":
				name += " " + tok
				found = True
				break
			elif ref.endswith('es') and tok == ref[:-2]:
				name += " " + tok
				found = True
				break
			elif ref.endswith('s') and tok == ref[:-1]:
				name += " " + tok
				found = True
				break
		if found == False:
			prop += " " + tok
	prop = prop.strip(", ")
	name = name.strip()
	if name == '':
		name = prop
		prop = ''
	return name,prop

def getAmountAndName(text):
	breakwords = {' cup ',' cups ',' tablespoon ',' tablespoons ',' teaspoon ',' teaspoons ',' ounce ',' ounces'}
	entity = {}
	entity['amount'] = ''
	entity['ingredient'] = text
	for word in breakwords:
		if (word) in text:
			splitText = text.split(word)
			entity['amount'] = (splitText[0] + word).strip()
			entity['ingredient'] = splitText[1]
			break
	entity['ingredient'],entity['properties'] = separateProperties(entity['ingredient'])
	entity['uri'] = getUri(entity)
	return entity

def getUri(data):
	if "ingredient" in data and "properties" in data:
		uri = data["ingredient"].lower() + " " + data["properties"].lower()
		uri = uri.replace("(","")
		uri = uri.replace(")","")
		uri = uri.replace(" ","_")
		uri = uri.strip("_")
		uri = "www.semanticfoodnetwork.com/" + uri
		return uri
	return ""

for substitute in data:
	substitute["ingredient"],substitute["properties"] = separateProperties(substitute["ingredient"])
	substitute["uri"] = getUri(substitute)
	for i in range(10):
		if "substitute"+str(i) in substitute:
			 sublist = getListOfSubstitutes(substitute["substitute"+str(i)])
			 entityList = []
			 for subst in sublist:
			 	entityList.append(getAmountAndName(subst))
			 substitute["substitute"+str(i)] = entityList

with open(outputfile,'w') as outf:
	json.dump(data,outf,indent=4,sort_keys=True)
