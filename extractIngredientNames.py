import json
import sys

filename = sys.argv[1]
#ingredientName = sys.argv[2]

with open(filename,'r') as f:
	data = json.load(f)

#for ingred in data["ingredients"]:
	#print ingred["name"] + ";" + ingred["properties"]

for ingred in data:
	print ingred["ingredient"] + ";" + ingred["properties"]
