import re
import sys

def fixQuantities(text):
	obj = re.search(r'(.*)(\d)(\D*)',text)
	if obj and obj.group(1) and obj.group(2) and obj.group(3):
		quantity = obj.group(1)+obj.group(2)
		metric = obj.group(3)
		tokens = quantity.split()
		numbers = []
		for tok in tokens:
			if "/" in tok:
				fraction = tok.split("/")
				if len(fraction) == 2:
					numbers.append(float(float(fraction[0])/float(fraction[1])))
				else:
					"Unparseable!"
			else:
				if tok.isdigit():
					numbers.append(float(tok))
				else:
					"Unparseable!"
		quantity = 0
		for num in numbers:
			quantity += num
		return str(quantity) + " " + metric
	return text

print fixQuantities("1/2 cups")
print fixQuantities("2 2/3 tablespoons")
