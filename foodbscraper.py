from bs4 import BeautifulSoup
from urllib import urlopen
from contextlib import closing
from selenium.webdriver import Firefox
import json
from pyvirtualdisplay import Display

display = Display(visible=0, size=(1024, 768))
display.start()

def get_fooddb_data():


    i = 1
    lst = []
    while i<= 1000:

        url = 'http://foodb.ca/foods/'+str(i)
        food = dict()
        nutrients = dict()

        i += 1

        try:
            # Get HTML Content 
            html = urlopen(url)
              # Setting BeautifulSoup
            soup = BeautifulSoup(html)

            tables = soup.findChildren('table')



            #get first table
            my_table = tables[0]



            nutrient = dict()

            rows = my_table.findChildren(['th', 'tr'])
            j =0

            #get name
            cells = rows[2].findChildren('td')
            food['Name'] = cells[0].string

            #get scientific name
            cells = rows[4].findChildren('td')
            food['ScientificName'] = cells[0].string

            #get description
            cells = rows[6].findChildren('td')
            food['Description'] = cells[0].string

            #get group
            cells = rows[12].findChildren('td')
            food['Group'] = cells[0].string

            #get Sub-Group
            cells = rows[6].findChildren('td')
            food['SubGroup'] = cells[0].string

            #get wiki link
            cells = rows[20].findChildren('td')
            for a in cells[0].find_all('a', href=True):
                food['WikipediaID']= a['href'].replace(' ','%20')

            #print(food)
            nutrient = list()
			
			#to get nutrients data we need to use selenium as the nutrient html is loaded from some js when scrolling!
            try:
                wd = Firefox()
                with closing(wd) as browser:
                    browser.get(url)
                    a = browser.find_element_by_id('DataTables_Table_1')
                    trs = a.find_elements_by_tag_name('tbody')[0].find_elements_by_tag_name('tr')

                    for tr in trs:
                        nutrients['link'] = tr.find_element_by_css_selector('a').get_attribute('href')
                        nutrients['name'] = tr.find_elements_by_tag_name('td')[0].text
                        nutrients['avgValue'] = tr.find_elements_by_tag_name('td')[2].text
                        nutrient.append(nutrients)

                    food['nutrients'] = nutrient


            except:
                print("no nutrients data")


        except:
            print("\t\tPage Parsing Error.")

        lst.append(food)

    f = open("completeFoodDB.json",'w')
    return json.dump(lst,f)




get_fooddb_data()
