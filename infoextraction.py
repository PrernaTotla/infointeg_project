import sys

origlist = sys.argv[1]
reference = sys.argv[2]
outputlist = sys.argv[3]

reference_tokens = []
ref_file = open(reference,'r')
line = ref_file.readline()
while line != None and line != '':
	tokens = line.strip().split()
	for tok in tokens:
		if tok not in reference_tokens:
			reference_tokens.append(tok)
	line = ref_file.readline()
#print len(reference_tokens)
ref_file.close()

def separateProperties(text):
	text = text.lower()
	text = text.replace(","," , ")
	name = ""
	prop = ""
	tokens = text.split()
	for tok in tokens:
		found = False
		for ref in reference_tokens:
			if tok == ref or tok == ref+"s" or tok == ref+"es":
				name += " " + tok
				found = True
				break
			elif ref.endswith('es') and tok == ref[:-2]:
				name += " " + tok
				found = True
				break
			elif ref.endswith('s') and tok == ref[:-1]:
				name += " " + tok
				found = True
				break
		if found == False:
			prop += " " + tok
	prop = prop.strip(", ")
	return name.strip(),prop

inf = open(origlist,'r')
outf = open(outputlist,'w')
outf.write("OriginalIngredient;IngredientName;Properties\n")
line = inf.readline()
i = 0
unique_ingredients = []
while line != None and line != '':
	line = line.strip()
	outf.write(line+";")
	name,prop = separateProperties(line)
	if name not in unique_ingredients:
		unique_ingredients.append(name)
	outf.write(name+";"+prop)
	outf.write("\n")
	i += 1
	if i%1000 == 0:
		print i
	line = inf.readline()
inf.close()
outf.close()

print len(unique_ingredients)
