__author__ = 'Rashmi'
import sys
import json

recipe = open(sys.argv[1],"r")
nutrient = open("nutrients_uri.json",'r')

nutr = json.load(nutrient)

recdata = json.load(recipe)

recipeingred = recdata["ingredients"]

calories = 0

#print(nutr)

output = dict()
not_calculated_nutrition = list()


tot_calories = 0
tot_fat = 0
tot_carbs = 0

for i in range(len(recipeingred)):
    all_nutrition = list()
    eachingred = recipeingred[i]
    for n in nutr:
        if eachingred['uri'] == n['uri']:
            #print(eachingred['ingredient'])
            #print(n['calories'])

            std_weight_ingred = eachingred['std_weight']
            std_weight_nutr = n['std_weight']

            if not n['calories'].isdigit():
                n['calories'] = 0
            if not n['carbs_grams'].isdigit():
                n['carbs_grams'] = 0
            if not n['fat_grams'].isdigit():
                n['fat_grams'] = 0

            nutrition = dict()

            #calculate nutrition only of both are numeric! fingers crossed!
            if not std_weight_ingred == "":
                if not std_weight_nutr == "":
                    nutrition['calories'] = (float(std_weight_ingred) * float(n['calories']))/float(std_weight_nutr)
                    nutrition['carbs_grams'] = (float(std_weight_ingred) * float(n['carbs_grams']))/float(std_weight_nutr)
                    nutrition['fat_grams'] = (float(std_weight_ingred) * float(n['fat_grams']))/float(std_weight_nutr)
                    #print("hello")
                else:
                    nutrition['calories'] = float(std_weight_ingred) * float(n['calories'])
                    nutrition['carbs_grams'] = float(std_weight_ingred) * float(n['carbs_grams'])
                    nutrition['fat_grams'] = float(std_weight_ingred) * float(n['fat_grams'])

                    tot_calories += nutrition['calories']
                    tot_carbs += nutrition['carbs_grams']
                    tot_fat += nutrition['fat_grams']
            all_nutrition.append(nutrition)

    output[eachingred['ingredient']] = all_nutrition
    if(len(all_nutrition)== 0):
        not_calculated_nutrition.append(eachingred['ingredient'])


print("*****NUTRITION**********")
print("tot_calories: "+str(tot_calories/100.00)+"%")
print("tot_fat: "+str(tot_carbs/100.00)+"%")
print("tot_carbs: "+str(tot_fat/100.00)+"%")

#print(output)

print("********not calculated*************")
print(not_calculated_nutrition)

