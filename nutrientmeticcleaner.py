__author__ = 'Rashmi'
import json

with open("nutrients_uri.json",'r') as f:
	data = json.load(f)

num_ingred = len(data["ingredients"])

i = 0

list_split = list()

#diameter, beef manual

while i<num_ingred:
    serving_size = data["ingredients"][i]["serving_size"].split(" ")
    x = dict()
    x = data["ingredients"][i]
    del x['serving_size']
    m = 0
    liststd = ["ounce","sheet","each","cup","ounces","sprigs","chop","pods", "cups","cherry","water", "teaspoon","bagel","slice","biscuits","grape","pomegranate","berries","mint", "tablespoon","full","package","olives","tablespoons","teaspoons","round","tripple","crackers","medium","large","small","piece","roll","pound","oz","clove","head","inner","pepper"]
    if(len(serving_size)==1):
        x["quantity"]= serving_size[0]
        list_split.append(x)

    else:
        while m<len(serving_size):
            k = 0
            if serving_size[m] in liststd:
                while k<len(liststd):
                    if serving_size[m] in liststd[k]:
                        x["quantity"] = serving_size[m-1]
                        x["metric"] = liststd[k]
                        list_split.append(x)
                        break
                    k += 1
            elif serving_size[m] =="12-ounce":
                x["quantity"] = "12"
                x["metric"] = "ounce"
                list_split.append(x)
            elif serving_size[m] =="2-ounce":
                x["quantity"] = "2"
                x["metric"] = "ounce"
                list_split.append(x)
            elif serving_size[m] =="ounces)":
                x["quantity"] = str(serving_size[m-1]).replace("(","")
                x["metric"] = "ounce"
                list_split.append(x)
            m += 1
    i += 1

print(len(list_split))
print(list_split)
