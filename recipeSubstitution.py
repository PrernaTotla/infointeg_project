import sys
import json

nutrientsFile = sys.argv[1]
substitutesFile = sys.argv[2]

nutrientsData = []
substitutesData = []

def getListOfSubstitutes(ingredient):
	if 'uri' in ingredient:
		for substitute in substitutesData:
			if ingredient['uri'] == substitute['uri']:
				#print "Found Match!!"
				listOfSubstitutes = []
				for i in range(10):
					if 'substitute'+str(i) in substitute:
						listOfSubstitutes.append(substitute['substitute'+str(i)])
				return listOfSubstitutes
	return []

def substituteIngredient(recipe,ingredient,sub_ingredient_list):
	ingredients = recipe['ingredients']
	ingredients.remove(ingredient)
	for sub_ingred in sub_ingredient_list:
		found = False
		for ingred in ingredients:
			if ingred['uri'] == sub_ingred['uri']:
				found = True
				print "Found in original recipe"
				ingred['std_weight'] += sub_ingred['std_weight']
				break
		if found == False:
			print "Not found in original recipe"
			sub_ingred['std_weight'] = ingredient['std_weight']
			sub_ingred['quantity'] = ingredient['quantity']
			ingredients.append(sub_ingred)
	return recipe

def test():
	recipeFile = "allrecipies-cleanedserving/101.json"
	with open(recipeFile,'r') as rf:
		recipeData = json.load(rf)
		subRecipe = recipeData
		ingredients = recipeData["ingredients"]
		for ingred in ingredients:
			sub_list = getListOfSubstitutes(ingred)
			if len(sub_list) == 0:
				print "No substitutes found!!  :::  " +ingred['uri']
			else:
				subRecipe = substituteIngredient(recipeData,ingred,sub_list[0])
				#json.dumps(subRecipe,indent=4,sort_keys=True)
				#print "\n\n"
				break
		print json.dumps(subRecipe,indent=4,sort_keys=True)

def loadData():
	global nutrientsData
	global substitutesData
	with open(nutrientsFile,'r') as nf:
		nutrientsData = json.load(nf)
		nutrientsData = nutrientsData['ingredients']
	with open(substitutesFile,'r') as sf:
		substitutesData = json.load(sf)

loadData()
test()