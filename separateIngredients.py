filename = "ingredientList.txt"

f = open(filename,'r',errors='ignore')
line = f.readline()
while line != None and line != '':
	line = line.strip()
	tokens = line.split(",",1)
	if len(tokens) < 2:
		print (line + ";" + tokens[0] + ";")
	else:
		print (line + ";" + tokens[0] + ";" + tokens[1])
	line = f.readline()
