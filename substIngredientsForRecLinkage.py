import sys
import json

inputfile = sys.argv[1]
outputfile = sys.argv[2]

unique = []

def extractIngredient(record):
	ingredient = record["ingredient"]
	properties = record["properties"]
	thisRecord = {"ingredient":ingredient,"properties":properties}
	if thisRecord not in unique:
		unique.append(thisRecord)

def extractSubstitute(record):
	for i in range(10):
		if "substitute"+str(i) in record:
			substList = record["substitute"+str(i)]
			for sub in substList:
				ingredient = sub["ingredient"]
				properties = sub["properties"]
				thisRecord = {"ingredient":ingredient,"properties":properties}
				if thisRecord not in unique:
					unique.append(thisRecord)

fin = open(inputfile,'r',errors='ignore')
data = json.load(fin)
for record in data:
	#extractIngredient(record)
	extractSubstitute(record)
fin.close()

fout = open(outputfile,'w')
fout.write("Ingredient;Property;CompleteName\n")
for ingredient in unique:
	fout.write(ingredient["ingredient"])
	fout.write(";")
	fout.write(ingredient["properties"])
	fout.write(";")
	fout.write(ingredient["ingredient"]+" "+ingredient["properties"])
	fout.write("\n")
fout.close()