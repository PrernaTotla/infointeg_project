ingred;prop;completename
almond;paste;almond paste
allspice;;allspice 
anchovies;;anchovies 
anise seed;seed;anise seed seed
anise;extract;anise extract
apple pie spice;;apple pie spice 
arrowroot;starch;arrowroot starch
baking powder;double actingrule: 1 teaspoon for every 1 cup flour;baking powder double actingrule: 1 teaspoon for every 1 cup flour
baking soda;(sodium bicarbonate)rule: 1/4 teaspoon for every 1 cup flour;baking soda (sodium bicarbonate)rule: 1/4 teaspoon for every 1 cup flour
bay leaf;;bay leaf 
beau monde;seasoning;beau monde seasoning
beer;;beer 
brandy;;brandy 
bread crumbs;dry;bread crumbs dry
bread crumbs;;bread crumbs 
broth;;broth 
beef;;beef 
chicken;;chicken 
butter;low-fat butter in baking;butter low-fat butter in baking
buttermilk;sour milk;buttermilk sour milk
cajun;spice;cajun spice
cake;flour;cake flour
cardamon;ground;cardamon ground
catsup;;catsup 
chervil;fresh;chervil fresh
chicken;stock base, instant;chicken stock base, instant
chicken;stock base, instant;chicken stock base, instant
chili;sauce;chili sauce
chives;finely chopped;chives finely chopped
chocolate chips;semi-sweet also check out chocolate substitution chart;chocolate chips semi-sweet also check out chocolate substitution chart
chocolate chips;semi-sweet chips, melted;chocolate chips semi-sweet chips, melted
chocolate;semi-sweet;chocolate semi-sweet
chocolate;unsweetened;chocolate unsweetened
chocolate white;;chocolate white 
cocoa powder;natural unsweetened;cocoa powder natural unsweetened
cocoa;dutch processed;cocoa dutch processed
coconut;;coconut 
coconut cream;canned;coconut cream canned
coconut milk;canned;coconut milk canned
coffee;strong brewed;coffee strong brewed
mushroom;condensed cream,soup;mushroom condensed cream,soup
cornmeal;self-rising;cornmeal self-rising
cornmeal;stone ground;cornmeal stone ground
corn;syrup;corn syrup
cornstarch;(for thickening);cornstarch (for thickening)
cheese;cottage;cheese cottage
cracker;crumbs;cracker crumbs
cream;half-and-half;cream half-and-half
cream;heavy (36 to 40% fat);cream heavy (36 to 40% fat)
cream;light (18 to 20% fat);cream light (18 to 20% fat)
cream;whipped;cream whipped
cheese;cream;cheese cream
cream;tartar;cream tartar
creme;fraiche;creme fraiche
currants;;currants 
dill plant;fresh or dried;dill plant fresh or dried
egg;;egg 
egg white;;egg white 
egg yolk;;egg yolk 
ash lemon;extracts;ash lemon extracts
mint;peppermint extracts;mint peppermint extracts
fish;sauce;fish sauce
flour;all-purpose (for thickening);flour all-purpose (for thickening)
all-purpose flour;note: specialty flours added to yeast bread recipes will result in a reduced volume and heavier product.;all-purpose flour note: specialty flours added to yeast bread recipes will result in a reduced volume and heavier product.
cake flour;;cake flour 
flour;self-rising;flour self-rising
whole wheat flour;;whole wheat flour 
garlic;;garlic 
garlic salt;;garlic salt 
gelatin;powdered;gelatin powdered
gelatin;plain;gelatin plain
gelatin;unflavored;gelatin unflavored
gelatin;leaf or sheet;gelatin leaf or sheet
gelatin;flavored;gelatin flavored
ghee;;ghee 
ginger;root, fresh;ginger root, fresh
onions;green fresh;onions green fresh
herbs;fresh;herbs fresh
herring;;herring 
honey;;honey 
horseradish;;horseradish 
pepper;hot sauce;pepper hot sauce
italian seasoning;;italian seasoning 
lemon;juice, freshly squeezed;lemon juice, freshly squeezed
lemon;whole;lemon whole
lemon;peel, dried;lemon peel, dried
lemon;zest (peel);lemon zest (peel)
lemon grass;;lemon grass 
lime;juice, freshly squeezed;lime juice, freshly squeezed
lime;zest (peel);lime zest (peel)
pasta;macaroni 4 cups cooked;pasta macaroni 4 cups cooked
maple sugar;;maple sugar 
maple syrup;;maple syrup 
marshmallows;miniature;marshmallows miniature
marshmallow;cr;marshmallow cr
mascarpone;;mascarpone 
mayonnaise;salads and salad dressings;mayonnaise salads and salad dressings
me yen seasoning;;me yen seasoning 
milk;evaporated;milk evaporated
milk;skim;milk skim
milk;sweetened condensed;milk sweetened condensed
milk;whole;milk whole
mint;leaves, fresh chopped;mint leaves, fresh chopped
molasses;;molasses 
mushrooms;fresh;mushrooms fresh
mustard;dry;mustard dry
mustard;dijon;mustard dijon
nuts;chopped, ground or whole;nuts chopped, ground or whole
oil;sauteing;oil sauteing
old bay seasoning;;old bay seasoning 
onion;fresh;onion fresh
orange;whole;orange whole
orange;juice, freshly squeezed;orange juice, freshly squeezed
orange;peel (zest), fresh;orange peel (zest), fresh
cheese;parmesan grated;cheese parmesan grated
parsley;fresh;parsley fresh
peanut butter;;peanut butter 
pepper;white;pepper white
bell peppers;green;bell peppers green
bell peppers;red;bell peppers red
peppermint;dried;peppermint dried
pimento;;pimento 
polenta;dry;polenta dry
poultry seasoning;;poultry seasoning 
pumpkin pie spice;;pumpkin pie spice 
raisins;;raisins 
rennet;;rennet 
rice;3 cups cooked;rice 3 cups cooked
cheese;ricotta;cheese ricotta
rum;;rum 
saffron;;saffron 
shortening;melted;shortening melted
shortening;solid baking;shortening solid baking
sour cream;cultured;sour cream cultured
spearmint;dried;spearmint dried
star anise;ground;star anise ground
sugar;brown;sugar brown
sugar;superfine;sugar superfine
sugar;white;sugar white
tahini;;tahini 
tapioca;instant or quick-cooking;tapioca instant or quick-cooking
tapioca;quick-cooking for thickening;tapioca quick-cooking for thickening
tomatoes;canned;tomatoes canned
tomatoes;fresh;tomatoes fresh
tomatoes;packed;tomatoes packed
tomato;juice;tomato juice
tomato;sauce;tomato sauce
tomato;soup;tomato soup
treacle;;treacle 
vanilla;bean;vanilla bean
vanilla;extract, pure;vanilla extract, pure
vegetable shortening;solid;vegetable shortening solid
vinegar;balsamic;vinegar balsamic
vinegar;regular white cider;vinegar regular white cider
worcestershire;sauce;worcestershire sauce
yeast;active dry;yeast active dry
yogurt;plain;yogurt plain
