import sys
import json

inputfile = sys.argv[1]
ingredsRecLinkage = sys.argv[2]
subsRecLinkage = sys.argv[3]
outputfile = sys.argv[4]

def createMap(filename):
	linkageMap = {}
	f = open(filename,'r')
	line = f.readline()
	line = f.readline()
	while line != None and line != '':
		columns = line.split(",")
		name = columns[0].strip('"')
		properties = columns[1].strip('"')
		matchedName = columns[3].strip('"')
		matchedProp = columns[4].strip('"')
		if name not in linkageMap:
			linkageMap[name] = []
		entity = {}
		entity['ingredient'] = name
		entity['properties'] = properties
		entity['matchedIngredient'] = matchedName
		entity['matchedProp'] = matchedProp
		linkageMap[name].append(entity)
		line = f.readline()
	f.close()
	return linkageMap

ingredientMap = createMap(ingredsRecLinkage)
substitutionsMap = createMap(subsRecLinkage)

f = open(inputfile,'r')
inputData = json.load(f)
f.close()

for ingredientData in inputData:
	# Replacing URI of ingredient with a "standard" URI
	if ingredientData['ingredient'] in ingredientMap:
		for possibleMatch in ingredientMap[ingredientData['ingredient']]:
			if ingredientData['properties'] == possibleMatch['properties']:
				#print "Found ingredient match: " + ingredientData['ingredient']+' '+ ingredientData['properties']
				uri = possibleMatch['matchedIngredient'] + ' ' + possibleMatch['matchedProp']
				uri = uri.replace('(','')
				uri = uri.replace(')','')
				uri = uri.replace(' ','_')
				uri = uri.strip('_')
				uri = "www.semanticfoodnetwork.com/" + uri
				ingredientData['uri'] = uri
				break
	# Adding "standard" URI to a substitute or creating a new one if needed
	for i in range(10):
		if "substitute"+str(i) in ingredientData:
			substList = ingredientData["substitute"+str(i)]
			for subst in substList:
				found = False
				if subst['ingredient'] in substitutionsMap:
					for possibleMatch in substitutionsMap[subst['ingredient']]:
						if subst['properties'] == possibleMatch['properties']:
							found = True
							uri = possibleMatch['matchedIngredient'] + ' ' + possibleMatch['matchedProp']
							uri = uri.replace('(','')
							uri = uri.replace(')','')
							uri = uri.replace(' ','_')
							uri = uri.strip('_')
							uri = "www.semanticfoodnetwork.com/" + uri
							subst['uri'] = uri
							break
				if found == False:
					uri = subst['ingredient'] + ' ' + subst['properties']
					uri = uri.replace('(','')
					uri = uri.replace(')','')
					uri = uri.replace(' ','_')
					uri = uri.strip('_')
					uri = "www.semanticfoodnetwork.com/" + uri
					subst['uri'] = uri

with open(outputfile,'w') as outf:
	json.dump(inputData,outf,indent=4,sort_keys=True)
